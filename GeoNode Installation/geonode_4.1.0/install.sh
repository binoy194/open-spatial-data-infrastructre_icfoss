# Install OpenJDK 11 JRE headless
sudo apt install openjdk-11-jre-headless

# Install Django 3.2.16 within the virtual environment
pip install Django==3.2.16

# Start the PostgreSQL service
sudo service postgresql start

# Check the status of the PostgreSQL service
#sudo service postgresql status


# Restart the PostgreSQL service to apply changes
#sudo service postgresql restart

django-admin startproject --template=./geonode-project -e py,sh,md,rst,json,yml,ini,env,sample,properties -n monitoring-cron -n Dockerfile opensdi410

cd opensdi410
cd src

pip install -r requirements.txt --upgrade
pip install -e . --upgrade
pip install -e git+https://github.com/GeoNode/geonode-importer.git#4.1.x#egg=geonode-importer

# Move dev configuration files to the project root
mv ../.override_dev_env.sample ../.override_dev_env

# Edit configurations file
nano ../.override_dev_env

# change these environment variables : 
# POSTGRES_USER={replace with postgres username}
# POSTGRES_PASSWORD={{replace with postgres password}
# GEONODE_DATABASE={replace with geonode database name : geonode}
# GEONODE_DATABASE_PASSWORD={replace with geonode database password}
# GEONODE_GEODATABASE={replace with geoserver database name : geonode_data}
# GEONODE_GEODATABASE_PASSWORD={replace with geoserver database password}
# DATABASE_HOST={postgres server ip OR localhost}
# DATABASE_PORT=5432
# DATABASE_URL=postgis://{geonode_database_username}:{geonode_database_password}@{postgres server ip}:5432/{geonode_database_name}
# GEODATABASE_URL=postgis://{geoserver_database_username}:{geoserver_database_password}@{postgres server ip}:5432/{geoserver_database_name}
# SITEURL=http://localhost/
# ALLOWED_HOSTS="['django', 'localhost']"
# GEOSERVER_ADMIN_USER=admin
# GEOSERVER_ADMIN_PASSWORD=admin
# ADMIN_USERNAME=admin
# ADMIN_PASSWORD=admin

mv manage_dev.sh.sample manage_dev.sh
mv paver_dev.sh.sample paver_dev.sh

source ../.override_dev_env

sh ./paver_dev.sh reset
sh ./paver_dev.sh setup
sh ./paver_dev.sh sync
sh ./paver_dev.sh start

